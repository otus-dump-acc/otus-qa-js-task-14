const { credentials, projectName, taskTitle } = require("./fixtures");

Feature('Favorites');

Before(({ LoginPage }) => {
  LoginPage.login(credentials)
})

const timestamp = Date.now()

Scenario('add new task to the project',  async ({ I, ProjectsPage, NewProjectPage, ProjectPage, FavoritesPage }) => {
  ProjectsPage.goto()
  ProjectsPage.openNewProjectAdding()
  NewProjectPage.addNewProject(projectName + timestamp)
  const projectURL = (await I.grabCurrentUrl()).split('/')
  const projectId = projectURL[projectURL.length - 2]
  ProjectPage.onPage(projectId, projectName + timestamp)
  ProjectPage.addNewTask(taskTitle + timestamp)
  ProjectPage.taskInList(taskTitle + timestamp)
  ProjectPage.addTaskToFavorites(taskTitle + timestamp)
  FavoritesPage.goto()
  FavoritesPage.taskInList(taskTitle + timestamp)
  ProjectPage.goto(projectId, projectName + timestamp)
  ProjectPage.removeProject(projectId)
});