const credentials = require("./fixtures").credentials;

Feature('Auth');

Scenario('user sign in and moved to the main page',  ({ LoginPage, MainPage }) => {
  LoginPage.login(credentials)
  MainPage.onPage()
  MainPage.welcomeTitleVisible(credentials.username)
});
