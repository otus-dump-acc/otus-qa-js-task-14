const {I} = inject()

module.exports = {
  projectTitle: locate('h1.project-title'),
  addNewTaskInput: locate('textarea.add-task-textarea'),
  addNewTaskButton: locate('button.add-task-button'),
  projectMenuButton: locate('button.project-title-button'),
  projectMenuDeleteButton:(projectId) => locate('.dropdown-content > a').withAttr({ href: `/projects/${projectId}/settings/delete` }),
  projectDeleteModalTitle: locate('.modal-content > .header > span'),
  projectDeleteSubmitButton: locate('.actions > button.is-primary'),

  onPage(id, projectName) {
    I.seeCurrentUrlEquals(`/projects/${id}/list`)
    I.see(projectName, this.projectTitle)
  },

  goto(id, projectName) {
    I.amOnPage(`https://try.vikunja.io/projects/${id}/list`)
  },

  addNewTask(taskText) {
    I.fillField(this.addNewTaskInput, taskText)
    I.click(this.addNewTaskButton)
    this.taskInList(taskText)
  },

  addTaskToFavorites(taskTitle) {
    const task = locate('.task').withText(taskTitle)
    I.see(taskTitle, task)
    I.moveCursorTo(task)
    const addToFavoritesButton = locate('.task > button.favorite')
    I.moveCursorTo(addToFavoritesButton)
    I.click(addToFavoritesButton)
  },

  removeProject(projectId) {
    I.click(this.projectMenuButton)
    I.click(this.projectMenuDeleteButton(projectId))
    I.seeElement(this.projectDeleteModalTitle)
    I.click(this.projectDeleteSubmitButton)
  },

  taskInList(taskText) {
    I.see(taskText, '.tasktext')
  }
}