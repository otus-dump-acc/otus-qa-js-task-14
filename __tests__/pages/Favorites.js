const {I} = inject()

module.exports = {
  projectName: locate('input').withAttr({ name: 'projectTitle' }),
  createButton: locate('button.is-primary'),

  onPage() {
    I.seeCurrentUrlEquals('/projects/-1/list')
  },

  goto() {
    I.amOnPage('https://try.vikunja.io/projects/-1/list')
    this.onPage()
  },

  taskInList(taskTitle) {
    I.see(taskTitle, 'ul.tasks')
  }
  
}