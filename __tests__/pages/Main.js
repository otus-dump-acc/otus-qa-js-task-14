const {I} = inject()

module.exports = {
  welcomeTitle: locate('h2'),

  onPage() {
    I.seeCurrentUrlEquals('/')
  },

  goto(username) {
    I.amOnPage('https://try.vikunja.io/')
    this.onPage()
    this.welcomeTitleVisible(username)
  },

  welcomeTitleVisible(username) {
    I.see(username, 'h2')
  },
}