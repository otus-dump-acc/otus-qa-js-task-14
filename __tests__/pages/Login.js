const {I} = inject()

module.exports = {
  usernameInput: locate('input#username'),
  passwordInput: locate('input#password'),
  loginButton: locate('button').withText('Login'),

  onPage() {
    I.seeCurrentUrlEquals('/login')
  },

  goto() {
    I.amOnPage('/login')
    I.see('Login', '.title')
  },

  fillUsername(username) { I.fillField(this.usernameInput, username) },
  fillPassword(password) { I.fillField(this.passwordInput, password) },
  submitLoginForm() { I.click(this.loginButton) },

  async login({ username, password }) {
    this.goto()
    this.fillUsername(username)
    this.fillPassword(password)
    this.submitLoginForm()
  },
}