const {I} = inject()

module.exports = {
  newProjectButton: locate('a.button').withAttr({href: '/projects/new'}),

  onPage() {
    I.seeCurrentUrlEquals('/projects')
  },

  goto() {
    I.amOnPage('https://try.vikunja.io/projects')
    this.onPage()
  },

  openNewProjectAdding(projectName) {
    I.click(this.newProjectButton)
  },

  projectNotInList(projectName) {
    I.dontSee(projectName, 'ul.project-grid')
  }
}