const {I} = inject()

module.exports = {
  projectName: locate('input').withAttr({ name: 'projectTitle' }),
  createButton: locate('button.is-primary'),

  onPage() {
    I.seeCurrentUrlEquals('/projects/new')
  },

  goto() {
    I.amOnPage('https://try.vikunja.io/projects/new')
    this.onPage()
  },

  addNewProject(projectName) {
    I.fillField(this.projectName, projectName)
    I.click(this.createButton)
  }
}