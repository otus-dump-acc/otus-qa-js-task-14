const { credentials, projectName, taskTitle } = require("./fixtures");

Feature('Project');

Before(({ LoginPage }) => {
  LoginPage.login(credentials)
})

const timestamp = Date.now()

Scenario('add new project',  async ({ I, ProjectsPage, NewProjectPage, ProjectPage }) => {
  ProjectsPage.goto()
  ProjectsPage.openNewProjectAdding()
  NewProjectPage.addNewProject(projectName + timestamp)
  const projectURL = (await I.grabCurrentUrl()).split('/')
  const projectId = projectURL[projectURL.length - 2]
  ProjectPage.onPage(projectId, projectName + timestamp)
  ProjectPage.removeProject(projectId)
});

Scenario('remove created project',  async ({ I, ProjectsPage, NewProjectPage, ProjectPage }) => {
  ProjectsPage.goto()
  ProjectsPage.openNewProjectAdding()
  NewProjectPage.addNewProject(projectName + timestamp)
  const projectURL = (await I.grabCurrentUrl()).split('/')
  const projectId = projectURL[projectURL.length - 2]
  ProjectPage.onPage(projectId, projectName + timestamp)
  ProjectPage.removeProject(projectId)
  ProjectsPage.goto()
  ProjectsPage.projectNotInList(projectName + timestamp)
});

Scenario('add new task to the project',  async ({ I, ProjectsPage, NewProjectPage, ProjectPage }) => {
  ProjectsPage.goto()
  ProjectsPage.openNewProjectAdding()
  NewProjectPage.addNewProject(projectName + timestamp)
  const projectURL = (await I.grabCurrentUrl()).split('/')
  const projectId = projectURL[projectURL.length - 2]
  ProjectPage.onPage(projectId, projectName + timestamp)
  ProjectPage.addNewTask(taskTitle + timestamp)
  ProjectPage.taskInList(taskTitle + timestamp)
  ProjectPage.removeProject(projectId)
});