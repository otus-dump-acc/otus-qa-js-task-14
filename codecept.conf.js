const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  tests: './__tests__/*.test.js',
  output: './output',
  emptyOutputFolder: true,
  helpers: {
    Playwright: {
      browser: 'chromium',
      url: 'https://try.vikunja.io',
      show: true,
      disableScreenshots: true
    }
  },
  include: {
    I: './steps_file.js',
    LoginPage: './__tests__/pages/Login.js',
    MainPage: './__tests__/pages/Main.js',
    ProjectsPage: './__tests__/pages/Projects.js',
    NewProjectPage: './__tests__/pages/NewProject.js',
    ProjectPage: './__tests__/pages/Project.js',
    FavoritesPage: './__tests__/pages/Favorites.js',
  },
  name: 'otus-qa-js-task-14'
}